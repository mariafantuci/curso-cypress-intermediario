/// <reference types="Cypress" />

Cypress.Commands.add("cloneViaHttp", (project) => {
  const domain = Cypress.config("baseUrl")

  cy.exec(
    `cd temp/ && git clone ${domain}${Cypress.env("user_name")}/${
      project.name
    }.git`
  );
  //git@localhost:root/project-20be6f94-fa61-42f5-905d-d6e4a4ae1ca2.git
});
